# TempServer: A python project for reading a DS18B20
# temperture sensor on a Raspberry Pi 
#
# thermometer.py: functions for setting up the DS18B20 and
# reading the data. Requires root access.
#
# temp_server.py: listens for an incoming request and returns
# the temperature data to the client. Requires root access.
#
# get_temp.py: sends a request to the server for a temperature
# and prints the result
#


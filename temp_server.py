#!/usr/bin/env python

import socket, sys
import thermometer as therm
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

HOST = sys.argv.pop() if len(sys.argv) == 2 else '127.0.0.1'
PORT = 1060


therm.setup_temp_modules()
room_temp = therm.setup_temp_file(0)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
while True:
    sc, sockname = s.accept()
    print 'Listening at', s.getsockname()
    print 'Incoming request from', sockname
    print 'Socket connects', sc.getsockname(), 'and', sc.getpeername()
    message = sc.recv(4)
    deg_c_room, deg_f_room = therm.read_temp(room_temp)
    sc.sendall(str(deg_f_room))
    sc.close()

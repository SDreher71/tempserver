#!/usr/bin/env python

import socket, sys
import thermometer as therm
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

HOST = sys.argv.pop() if len(sys.argv) == 2 else '127.0.0.1'
PORT = 1060

s.connect((HOST, PORT))
s.sendall('temp')

reply = s.recv(16)
print repr(float(reply))
s.close()

import os
import glob
import time

def setup_temp_modules():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')

def setup_temp_file(therm):    
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + '28*')[therm]
    device_file = device_folder + '/w1_slave'
    return device_file

def read_temp_raw(dev_file):
    f = open(dev_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp(dev_file):
    lines = read_temp_raw(dev_file)
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(dev_file)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f
	

